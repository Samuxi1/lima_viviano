#ifndef FUNCTION_H
#define FUNCTION_H

//Prototipi
class Circonferenza {
    private:
        double centroX; // Coordinata X del centro
        double centroY; // Coordinata Y del centro
        double raggio;
    public:
        Circonferenza(double x, double y, double r);
        void getCentro(double x, double y);
        double getRaggio();
        void setCentro(double x, double y);
        void setRaggio(double r);
        void stampaDati();
        void calcolaArea();
        void calcolaPerimetro();
};
#endif  

#include <iostream>
#include "function.h"

using namespace std;

int main(){
  // Creazione di un oggetto Circonferenza
  Circonferenza circonferenza(2, 3, 5);
  // Stampa dei dati della circonferenza
  circonferenza.stampaDati();
  cout << "=====Calcolo area====="<<endl;
  circonferenza.calcolaArea();
  cout << "=====Calcolo perimetro====="<<endl;
  circonferenza.calcolaPerimetro();
  // Setto il raggio diverso
  cout << "=====STAMPA NUOVI PARAMETRI======"<<endl;
  circonferenza.setRaggio(10);
  circonferenza.setCentro(4,5);
  circonferenza.stampaDati();
  cout << "=====Calcolo area====="<<endl;
  circonferenza.calcolaArea();
  cout << "=====Calcolo perimetro====="<<endl;
  circonferenza.calcolaPerimetro();
  return 0;
}


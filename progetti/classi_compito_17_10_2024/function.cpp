#include "function.h"
#include <iostream>
using namespace std;

//Implementazione

// Costruttore
    Circonferenza::Circonferenza(double x, double y, double r) : centroX(x), centroY(y), raggio(r) {}
// Metodo per ottenere le coordinate del centro
    void Circonferenza::getCentro(double x, double y){
        x = centroX;
        y = centroY;
    }
// Metodo per ottenere il raggio
    double Circonferenza::getRaggio(){
        return raggio;
    }
// Metodo per impostare il centro
    void Circonferenza::setCentro(double x, double y){
    centroX = x;
    centroY = y;
    }
// Metodo per impostare il raggio
    void Circonferenza::setRaggio(double r){
    if (r >= 0) {
        raggio = r;
    } else {
        cout << "Il raggio deve essere positivo." <<endl;
    }
    }
// Metodo per stampare i dati della circonferenza
    void Circonferenza::stampaDati(){
    cout << "Centro: (" << centroX << ", " << centroY << ")\n";
    cout << "Raggio: " << raggio << "\n";
    }
// Metodo per calcolare l'area
    void Circonferenza::calcolaArea() {
    double area;
    area = 3.14 * raggio * raggio;
    cout << "Area:" << area <<endl;
    }
// Metodo per calcolare il perimetro
    void Circonferenza::calcolaPerimetro(){
    double perimetro;
    perimetro = 2 * 3.14 * raggio;
    cout <<"Perimetro: " << perimetro <<endl;
    }


#ifndef FUNCTION_H
#define FUNCTION_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;

// Strutture dati
struct Domanda {
    string testo;
    double valutazione;
};

struct ListaDomande {
    Domanda dato;
    struct ListaDomande *next;
};

struct Voto {
    string materia;
    string data;
    double numero; // media calcolata
    ListaDomande *domande;
};

struct ListaVoti {
    Voto dato;
    struct ListaVoti *next;
};

struct Persona {
    string nome;
    string cognome;
    int anno;
    string classe;
    struct ListaVoti *voti;
};

struct ListaPersone {
    Persona dato;
    struct ListaPersone *next;
};

// Prototipi delle funzioni
void stampa(const ListaPersone *);
void stampa(const ListaVoti *);
void stampa(const ListaDomande *);
void stampa(Persona);
void stampa(Voto);

string get_nome();
string get_cognome();
string get_data();
string get_classe();
double get_valore_voto();
string get_materia();
int get_anno();

Persona get_persona();
Voto get_voto();
ListaVoti *get_lista_voti();
ListaDomande *get_lista_domande();
ListaPersone *get_lista_persone();

int get_casuale_tra(int, int);

// Funzione per creare il file HTML
void crea_html(const ListaPersone *);

// Funzione per stampare i voti in HTML
void stampa_html(const ListaVoti *l, ofstream &file);

// Funzione per stampare le domande in HTML
void stampa_domande_html(const ListaDomande *l, ofstream &file);


#endif

#include "function.h"

int main() {
    srand((unsigned int)time(NULL));
    cout << "Esercizio su alunni e voti" << '\n' << endl;

    // Crea la lista degli alunni
    ListaPersone *classe = get_lista_persone();

    // Stampa sul terminale
    stampa(classe);

    // Crea il file HTML con i dati
    crea_html(classe);



    return 0;
}

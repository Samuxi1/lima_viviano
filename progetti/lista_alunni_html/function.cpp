#include "function.h"

// Funzione per ottenere un numero casuale tra due valori
int get_casuale_tra(int da, int a) {
    return (rand() % (a - da + 1)) + da;
}

// Funzione per ottenere un nome casuale
string get_nome() {
    string tmp[] = {"Luca", "Paolo", "Francesca", "Laura", "Camilla"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

string get_cognome() {
    string tmp[] = {"Lucchini", "Meloni", "Silvestri", "Bennato", "Verdi", "Rossi"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

string get_data() {
    return to_string(get_casuale_tra(1, 28)) + "/" + to_string(get_casuale_tra(1, 12)) + "/" + to_string(get_casuale_tra(2000, 2024));
}

string get_classe() {
    string tmp[] = {"1ait", "2bit", "3cit", "4ait", "4bit", "4cafm", "3bcat", "2aafm"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

double get_valore_voto() {
    return ((double)get_casuale_tra(4, 20)) / 2.0;
}

string get_materia() {
    string tmp[] = {"Informatica", "Matematica", "Italiano", "Storia", "Scienze motorie", "Francese"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

int get_anno() {
    return get_casuale_tra(1960, 2020);
}

Persona get_persona() {
    Persona p;
    p.nome = get_nome();
    p.cognome = get_cognome();
    p.anno = get_anno();
    p.classe = get_classe();
    p.voti = get_lista_voti();
    return p;
}

Voto get_voto() {
    Voto v;
    v.materia = get_materia();
    v.data = get_data();
    v.domande = get_lista_domande();

    // Calcolo della media
    double somma = 0;
    int count = 0;
    ListaDomande *curr = v.domande;
    while (curr != NULL) {
        somma += curr->dato.valutazione;
        count++;
        curr = curr->next;
    }
    v.numero = somma / count;
    return v;
}

ListaDomande* get_lista_domande() {
    ListaDomande *ans = new ListaDomande;
    ans->dato = {"Domanda 1", get_valore_voto()};
    ans->next = NULL;

    ListaDomande *tmp = ans;
    for (int i = 1; i < get_casuale_tra(3, 5); i++) {
        tmp->next = new ListaDomande;
        tmp = tmp->next;
        tmp->dato = {"Domanda " + to_string(i + 1), get_valore_voto()};
        tmp->next = NULL;
    }
    return ans;
}

ListaVoti* get_lista_voti() {
    ListaVoti *ans = new ListaVoti;
    ans->dato = get_voto();
    ans->next = NULL;

    ListaVoti *tmp = ans;
    for (int i = 0; i < get_casuale_tra(5, 10); i++) {
        tmp->next = new ListaVoti;
        tmp = tmp->next;
        tmp->dato = get_voto();
        tmp->next = NULL;
    }
    return ans;
}

ListaPersone* get_lista_persone() {
    ListaPersone *ans = new ListaPersone;
    ans->dato = get_persona();
    ans->next = NULL;

    ListaPersone *tmp = ans;
    for (int i = 0; i < get_casuale_tra(1, 5); i++) {
        tmp->next = new ListaPersone;
        tmp = tmp->next;
        tmp->dato = get_persona();
        tmp->next = NULL;
    }
    return ans;
}

// Funzione per stampare una lista di persone
void stampa(const ListaPersone *l) {
    if (l != NULL) {
        stampa(l->dato);    // Stampa i dettagli della persona
        stampa(l->next);    // Chiamata ricorsiva per la persona successiva
    }
}

// Funzione per stampare una persona
void stampa(Persona p) {
    cout << "======= Persona =============" << endl;
    cout << "Nome: " << p.nome << "; Cognome: " << p.cognome << "; Anno: " << p.anno << "; Classe: " << p.classe << endl;
    cout << "\t======= Voti =============" << endl;
    stampa(p.voti);  // Stampa i voti associati alla persona
}

// Funzione per stampare una lista di voti
void stampa(const ListaVoti *l) {
    if (l != NULL) {
        stampa(l->dato);    // Stampa il voto
        stampa(l->next);    // Chiamata ricorsiva per il voto successivo
    }
}

// Funzione per stampare un voto
void stampa(Voto v) {
    cout << "\tMateria: " << v.materia << "; Data: " << v.data << "; Voto (Media): " << v.numero << endl;
    cout << "\t\tDettaglio domande:" << endl;
    stampa(v.domande);  // Stampa le domande associate al voto
}

// Funzione per stampare una lista di domande
void stampa(const ListaDomande *l) {
    if (l != NULL) {
        cout << "\t\t" << l->dato.testo << ": " << l->dato.valutazione << endl;
        stampa(l->next);    // Chiamata ricorsiva per la domanda successiva
    }
}

// Funzione per creare il file HTML
void crea_html(const ListaPersone *l) {
    ofstream file("alunni_voti.html");

    file << "<!DOCTYPE html>" << endl;
    file << "<html lang=\"it\">" << endl;
    file << "<head>" << endl;
    file << "<meta charset=\"UTF-8\">" << endl;
    file << "<title>Elenco Alunni e Voti</title>" << endl;
    file << "</head>" << endl;
    file << "<body>" << endl;

    file << "<h1>Elenco Alunni e Voti</h1>" << endl;
    file << "</body>" << endl;
    // Stampa lista persone
    if (l != NULL) {
        while (l != NULL) {
            file << "<h2>Alunno: " << l->dato.nome << " " << l->dato.cognome << "</h2>" << endl;
            file << "<p><strong>Classe:</strong> " << l->dato.classe << "; <strong>Anno:</strong> " << l->dato.anno << "</p>" << endl;

            file << "<h3>Voti</h3>";
            file << "<table><tr><th>Materia</th><th>Data</th><th>Voto (Media)</th><th>Domande</th></tr>";
            stampa_html(l->dato.voti, file);
            file << "</table>";

            l = l->next;
        }
    }

    file << "</body>" << endl;
    file << "</html>" << endl;

    file.close();
}

// Funzione per scrivere i voti e domande in HTML
void stampa_html(const ListaVoti *l, ofstream &file) {
    if (l != NULL) {
        file << "<tr><td>" << l->dato.materia << "</td><td>" << l->dato.data << "</td><td>" << l->dato.numero << "</td><td>" << endl;
        file << "<div class=\"voti\">";
        stampa_domande_html(l->dato.domande, file);
        file << "</div></td></tr>";
        stampa_html(l->next, file);
    }
}

// Funzione per scrivere le domande in HTML
void stampa_domande_html(const ListaDomande *l, ofstream &file) {
    if (l != NULL) {
        file << l->dato.testo << " (Valutazione: " << l->dato.valutazione << ")<br>";
        stampa_domande_html(l->next, file);
    }
}


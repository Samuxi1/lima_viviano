#include <iostream>
using namespace std;
// prototipi
int get_int(string messaggio, int min, int max);
int casuale_tra(int min, int max);

int main () {
   int i, j, n=10;
   srand ((unsigned int)time (NULL)); //inizializzo il seme con cast delle variabili corretta
   n= get_int ("Dimmi fino a quale numero stampare la tabellina ", 1 ,10);
   cout << "Ecco la tabellina fino al numero " << n <<endl;
   for (i=0; i<=n; i++){
     for (j=0; j<=n; j++){
        cout << i+j << "; ";
        }
        cout << endl;
    } 
    return 0;    
}

//Implementazione
int get_int(string messaggio, int min, int max){
  int ans = 0;
  do {
  cout << "NB: valori compresi tra " << min << " e " << max <<endl;
  cout << messaggio;
  //chiedo il valore
  cin >> ans;
  //controllo che sia nel range
  }while (ans < min || ans >max );
  //restituisco il valore
    return ans;
}

int casuale_tra(int min, int max) {
   int ans=0;
   if (min>max) {
    ans= casuale_tra(max, min); //esempio di ricorsione
   }else {
    ans= rand()%(max-min+1)+min;
    }
    return ans;
}


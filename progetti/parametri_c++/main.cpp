#include <iostream>
using namespace std;

// Prototipo

void help(int argc, char** argv);

// Utilizzo

int main(int argc, char** argv){
    if (argc <= 1){
        help(argc,argv);
    }else{
       cout << " Dal main il numero di argomenti " << argc -1 <<endl;
       for (int i=0; i<argc; i++){
         if (i==0){
            cout << " Ciao io sono " << argv[0]<<endl;   
         }else {
             cout << "=====STAMPAMELO====="<<endl;
             cout << " argomento numero " << i << "==" << argv[i]<<endl;
          }             
        } 
     }
    return 0;
}

// Implementazione

void help(int argc, char** argv){
    cout << " Per usarmi devi usare " << argv[0] << " Argomento1, Argomento2, Argomento3 ed io le stampo a video "<<endl;
}

```mermaid
flowchart TD

    inizio([Inizio])
    fine([Fine])
    n(int numero)
    numero[/cin: numero/]
    pari{Pari?}
    stampaPari[/cout: "numero pari"/]
    multiplo{Multiplo di 5?}
    stampaMultiplo[/cout: "numero è multiplo"/]

    inizio-->|dichiaro numero|n
    n--> numero
    numero-->pari
    pari-->|si|stampaPari
    pari-->|no|numero
    stampaPari-->multiplo
    multiplo-->|si|stampaMultiplo
    multiplo-->|no|numero
    stampaMultiplo-->fine
```
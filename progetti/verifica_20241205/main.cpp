#include <iostream>

using namespace std;
// Costanti
#define N_ANIMALI 10

// strutture

struct Animale {
  string nome;
  string razza;
};

struct ListaAnimali {
  Animale dato;
  ListaAnimali *next;
};

// Prototipi
void stampa(Animale);
void stampa(const ListaAnimali *);
string get_nome(); // Restituisce un nome di animale a caso
string get_razza();  // Restituisce una razza a caso
Animale get_animale(); // Restituisce un animale con nome e razza presi a caso
ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di `N_ANIMALI` animali

int main() {
  srand((unsigned int)time(NULL));
  ListaAnimali *amici;
  cout <<"numero di animali: " << N_ANIMALI <<endl;
  amici = get_lista_animali();
  stampa(amici);
  return 0;
}

// Implementazione

void stampa(Animale a){
  cout << "\tNome: " << a.nome << "\tRazza: " << a.razza <<endl;
}
void stampa(const ListaAnimali * l){
  if(l != NULL){
    stampa(l->dato);
    stampa(l->next);
  }else{
    cout << "===Fine Lista===" <<endl;
  }
}
string get_nome(){
  string ans;
  string tmp[] = {"Bob", "Miao", "Bianchina", "Fido", "Fuffi"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
string get_razza(){
  string ans;
  string tmp[] = {"Barboncino", "Rotvailer","Cavalier king", "Bassotto", "Buldog"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
Animale get_animale(){
  Animale ans;
  ans.nome = get_nome();
  ans.razza = get_razza();
  return ans;
}
ListaAnimali * get_lista_animali(){
  ListaAnimali *ans = NULL, *tmp;
  ans = new (ListaAnimali);
  ans->dato = get_animale();
  ans->next = NULL;
  tmp = ans;
  for(int i=1; i < N_ANIMALI; i++) {
    tmp->next = new(ListaAnimali);
    tmp = tmp->next;
    tmp->dato = get_animale();
    tmp->next = NULL;
  }
  return ans;
}

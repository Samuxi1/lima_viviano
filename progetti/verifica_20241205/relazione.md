# Compito di informatica del 05/12/2024

Autore: Samuele Viviano

## Consegna

Si vuole realizzare un programma che stampi a video il contenuto di una lista di animali domestici (nome e razza) uno per riga.

I nomi e le razze vengono generati in modo casuale. Il numero di elementi inseriti e stampati deve essere anch'esso casuale e compreso tra 1 a 12.

Qualcuno ha abbozzato una traccia del programma, tu hai il compito di terminarlo e di relazionarne il funzionamento.

Descrivi il comportamento iniziale del programma riportando l'output ottenuto e il comando utilizzato per compilare.

Completa il codice c++ del file main.cpp e crea una relazione sul suo funzionamento.

Il codice attualmente compila.

La directory si comporta come un archivio git (pull e push non sono configurati e non servono) pretanto puoi utilizzare
gli altri comandi git che conosci per controllare il lavoro fatto ma non dimenticarti di riportarli nella relazione,
utlizzali per migliorare il tuo lavoro di relazione.

Considera che il codice non � stato testato quindi potrebbe essere necessario effettuare modifiche anche sostanziali al programma.

Puoi decidere di rifare da zero il codice.

Inserisci il codice sorgente del `main.cpp` tra gli allegati.

## Svolgimento
### Comportamento iniziale del programma
Il programma inizialmente assegna un nome all'animale tra quelli disponibile cioè "Bob "Miao, "Bianchina "Fido" e "Fuffi" e indica il numero di animali presenti nella lista.
### Output programma iniziale
Per compilare ho utilizzato il comado make visto che nel progetto è presente il makefile.
```c++
Debug: Fido; numero animali: 10
I'm `void stampa(const ListaAnimali * l)`: Lavori in corso
```
### stampa(Animale a)
```c++
void stampa(Animale a){
  cout << "\tNome: " << a.nome << "\tRazza: " << a.razza <<endl;
}
```
### stampa(const ListaAnimali * l)
```c++
void stampa(const ListaAnimali * l){
  if(l != NULL){
    stampa(l->dato);
    stampa(l->next);
  }else{
    cout << "===Fine Lista===" <<endl;
  }
}
```
### get_razza()
```c++
string get_razza(){
  string ans;
  string tmp[] = {"Barboncino", "Rotvailer","Cavalier king", "Bassotto", "Buldog"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
```
### get_animale()
```c++
Animale get_animale(){
  Animale ans;
  ans.nome = get_nome();
  ans.razza = get_razza();
  return ans;
}
```
### Output programma
Il programma assegna a 10 animali presenti nella lista un nome e una razza in modo casuale tra quelli dati.
```c++
numero di animali: 10
        Nome: Fido      Razza: Barboncino
        Nome: Fuffi     Razza: Bassotto
        Nome: Bianchina Razza: Buldog
        Nome: Miao      Razza: Buldog
        Nome: Miao      Razza: Cavalier king
        Nome: Fuffi     Razza: Cavalier king
        Nome: Miao      Razza: Barboncino
        Nome: Fido      Razza: Rotvailer
        Nome: Bob       Razza: Bassotto
        Nome: Fuffi     Razza: Buldog
===Fine Lista===
```
## Allegati

### Contenuto del `main.cpp`

```c++
// strutture

struct Animale {
  string nome;
  string razza;
};

struct ListaAnimali {
  Animale dato;
  ListaAnimali *next;
};

// Prototipi
void stampa(Animale);
void stampa(const ListaAnimali *);
string get_nome(); // Restituisce un nome di animale a caso
string get_razza();  // Restituisce una razza a caso
Animale get_animale(); // Restituisce un animale con nome e razza presi a caso
ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di `N_ANIMALI` animali

int main() {
  srand((unsigned int)time(NULL));
  ListaAnimali *amici;
  cout <<"numero di animali: " << N_ANIMALI <<endl;
  amici = get_lista_animali();
  stampa(amici);
  return 0;
}

// Implementazione

void stampa(Animale a){
  cout << "\tNome: " << a.nome << "\tRazza: " << a.razza <<endl;
}
void stampa(const ListaAnimali * l){
  if(l != NULL){
    stampa(l->dato);
    stampa(l->next);
  }else{
    cout << "===Fine Lista===" <<endl;
  }
}
string get_nome(){
  string ans;
  string tmp[] = {"Bob", "Miao", "Bianchina", "Fido", "Fuffi"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
string get_razza(){
  string ans;
  string tmp[] = {"Barboncino", "Rotvailer","Cavalier king", "Bassotto", "Buldog"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
Animale get_animale(){
  Animale ans;
  ans.nome = get_nome();
  ans.razza = get_razza();
  return ans;
}
ListaAnimali * get_lista_animali(){
  ListaAnimali *ans = NULL, *tmp;
  ans = new (ListaAnimali);
  ans->dato = get_animale();
  ans->next = NULL;
  tmp = ans;
  for(int i=1; i < N_ANIMALI; i++) {
    tmp->next = new(ListaAnimali);
    tmp = tmp->next;
    tmp->dato = get_animale();
    tmp->next = NULL;
  }
  return ans;
}

```
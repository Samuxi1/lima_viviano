#ifndef FUNCTION_H
#define FUNCTION_H

#include <iostream>
#include <string>
#include <fstream> 
#include <cstdlib> 
#include <ctime> 

using namespace std;

// Classe PaginaHtml
class HtmlFileManager {
public:
    HtmlFileManager(const string& fileName);
    void createFile();
    bool fileExist() const;
    static void showHelp();

private:
    string fileName;
};


#endif

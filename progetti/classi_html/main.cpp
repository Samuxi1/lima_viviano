#include "function.h"
int main(int argc, char** argv) {
    if (argc == 2) {
        HtmlFileManager manager(argv[1]);
        if (manager.fileExist()) {
            cout << "File già esistente non sovrascrivo." << endl;
        } else {
            manager.createFile();
        }
    } else {
        HtmlFileManager::showHelp();
    }
    return 0;
}
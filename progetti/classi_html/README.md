Nome del programma
==================

Autore: Picchiottino Roberto
Scopo: file di default da modificare
Data: 02/11/2022
Aggiornamento: gennaio 2025

## Scopo del progetto 

Utilizzare delle classi a scopo didattico.

## Osservazioni e approfondimenti

## Fonti

## Allegati

## Info utili

- README: questo file
- INSTALL: comandi e info su istallazione, compilazione ed esecuzione.
- COPYING: licenza d'uso.

#include "function.h"


HtmlFileManager::HtmlFileManager(const string& fileName) : fileName(fileName) {}

void HtmlFileManager::createFile() {
    string fullFileName = fileName + ".html";
    cout << "Creazione file: " << fullFileName << endl;
    ofstream wfile(fullFileName);
    wfile << "<!DOCTYPE html>" << endl;
    wfile << "<html>" << endl;
    wfile << "<head>" << endl;
    wfile << "<title>" << "Ciao Mondo" << "</title>" << endl;
    wfile << "</head>" << endl;
    wfile << "<body>" << endl;
    wfile << "<h1>Hello World</h1>" << endl;
    wfile << "<p>Welcome!</p>" << endl;
    wfile << "</body>" << endl;
    wfile << "</html>" << endl;
    wfile.close();
}

bool HtmlFileManager::fileExist() const {
    string fullFileName = fileName + ".html";
    ifstream rfile(fullFileName);
    bool exists = rfile.is_open();
    rfile.close();
    return exists;
}

void HtmlFileManager::showHelp() {
    cout << "Prova: ./program file_name" << endl;
    cout << "Usa solo il file_name senza l'estensione, verrà aggiunta automaticamente." << endl;
}
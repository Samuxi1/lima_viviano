#include "function.h"


int main(int argc,char * argv[]){
  int dimensione;
  string file;
  cout<<" Ciao, sono il tuo programma per caricare i dati di un appartamento in vendita "<<endl;
  appartamento casa = inserisci_appartamento(); 
  stampa(casa) ;
  cout << "Quanti appartamenti vuoi caricare ? " <<endl;
  cin >> dimensione;
  appartamento v_casa[dimensione];
  inserisci_appartamenti(v_casa,dimensione);
  cout << "==========STAMPA DIVERSE CASE============"<<endl;
  stampa_case(v_casa,dimensione);
  cout << "=========STAMPA CASE DA FILE============"<<endl;
  if(argc > 1){
    file = argv[1];
    cout<<file<<endl; 
  }
  bool lock = apri_file(file);
  if (lock == true){
    cout<< "=====DATI CASE======"<<endl;
    int a = conta_righe(file);
    cout<<"righe file: "<< a<<endl;
    cout << "===================="<<endl;
  }
  cout << "=============STAMPA CASE ORDINATE PER PREZZO============="<<endl;
  ordina(v_casa,dimensione);
  stampa_case(v_casa, dimensione);
  return 0;
}


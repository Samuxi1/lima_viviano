#include "function.h"

using namespace std;


#include <iostream>

//Implementazione
appartamento inserisci_appartamento(){
	appartamento casa;
	cout<<"inserisci il numero delle stanze " <<endl ;
	cin >>casa.stanze ;
	cout<<"inserisci i metri quadri "<<endl ;
	cin>>casa.metri ;
	cout<<"inserisci numero di bagni "<<endl;
	cin>>casa.bagni;
    cout<<"inserisci il prezzo di vendita "<<endl;
	cin>>casa.prezzo;
    cout<<"inserisci il comune "<<endl;
	cin>>casa.comune;
    cout<<"inserisci numero di box "<<endl;
	cin>>casa.box;
	cout<<"========================================"<<endl;
	return casa;
}
void inserisci_appartamenti(appartamento v_casa[],int dimensione){
	for(int i=0;i<dimensione;i++){
		v_casa[i]=inserisci_appartamento();
	}
}
void stampa(appartamento casa){  
    cout << "============STAMPA============"<<endl;
	cout<<" numero di stanze: " <<casa.stanze<<endl ;
	cout<<" metri quadrati: "<<casa.metri <<endl ;
	cout<<" numero di bagni: " <<casa.bagni <<endl ;
    cout<<" prezzo di vendita: " <<casa.prezzo <<" euro"<<endl ;
	cout<<" nome del comune: " <<casa.bagni <<endl ;
    cout<<" numero di box: " <<casa.box <<endl ;
	cout << "=============================="<<endl;
}	
void stampa_case(const appartamento v_casa[], int dimensione){
	for(int i=0; i < dimensione; i++){
		stampa(v_casa[i]);
	}
}
bool apri_file(string nome_file){
    bool ans = false ;
    ifstream n_file;
    n_file.open(nome_file);
    if (n_file.is_open()){
        cout<<"file aperto"<<endl;
        ans = true ;
    }
    else{
    cout<<"non riesco ad aprire il file"<<nome_file<<endl;
    }
    n_file.close();
    return ans;
  }
  
int conta_righe(string nome_txt){
  int ans=0;
  string line;
  ifstream n_file;
  n_file.open(nome_txt);
  if (n_file.is_open()){
        while(getline(n_file,line)){
        cout <<line<<endl;
        ans++;
        }
    n_file.close();
    }
return ans;
}
void ordina(appartamento v_casa[], int dimensione){
    for(int i=0; i< dimensione; i++){
        for(int k=i+1; k< dimensione; k++){
            if(v_casa[i].prezzo > v_casa[k].prezzo){
                scambia(v_casa[i], v_casa[k]);
            }
            if(v_casa[i].stanze > v_casa[k].stanze){
                scambia(v_casa[i], v_casa[k]);
            }
        }
    }
}
void scambia(appartamento &casa, appartamento &casa2){
    appartamento tmp;
    tmp = casa2;
    casa2 = casa;
    casa = tmp;
}
#ifndef _FUNCTION_H
#define _FUNCTION_H
using namespace std;
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

//Prototipo
struct appartamento{
    int stanze;
    int metri;
    int bagni;
    float prezzo;
    string comune;
    int box;
};

appartamento inserisci_appartamento();
void inserisci_appartamenti(appartamento v_casa[],int dimensione);
void stampa_case(const appartamento v_casa[], int dimensione);
void stampa(appartamento casa);
bool apri_file(string nome_file);
int conta_righe(string nome_txt);
void ordina(appartamento v_casa[], int dimensione);
void scambia(appartamento &casa, appartamento &casa2);

#endif  

#include "function.h"


int main(){
  Counter c; 
  cout << c.getValue() << endl;
  c.increment(5);
  cout << c.getValue() << endl;
  cout << "====PROGRAMMA 2========"<<endl;
  Counters c1(10); 
  Counters *c2 = new Counters(20);
  cout << c1.getValue() << endl;
  c1.increment(5);
  cout << c1.getValue() << endl;
  cout << c2->getValue() << endl;
  c2->increment(5);
  cout << c2->getValue() << endl;
  delete c2;
}


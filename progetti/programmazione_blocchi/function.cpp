#include "function.h"

using namespace std;


#include <iostream>

//Implementazione

Counter::Counter() {
    value = 0;  
}

void Counter::increment(int n) {
    value += n; 
}
                
int Counter::getValue() {
    return value;
}
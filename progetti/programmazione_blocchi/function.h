#ifndef _FUNCTION_H
#define _FUNCTION_H
using namespace std;
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

//Prototipo
class Counter {
    public:
        Counter();      // Costruttore 
        int getValue(); 
        void increment(int n);                  
    private:
        int value;  
}; // fine dichiarazione:nota il ;

class Counters {
    public:
        Counters():value(0){}
        Counters(int n):value(n){}
        int getValue() {
         return value; } 
        void increment(int n){value += n;}                  
    private:
        int value;  
}; 
#endif  

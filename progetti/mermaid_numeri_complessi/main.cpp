#include <iostream>
#include "function.h"

using namespace std;

int main() {
  Complessi numero(1,1);
  Complessi numero2(3,4);
  Complessi numero3;
  numero.stampaCoordinate();
  numero2.stampaCoordinate();
  numero3 = numero.add(numero, numero2);
  cout << "Coordinate sommate:" <<endl;
  numero3.stampaCoordinate();
  return 0;
}


#include "function.h"
#include <iostream>
#include <cmath>

using namespace std;

// Costruttore
Complessi::Complessi(int reale, int immaginaria) {
    nReale = reale;
    nImmaginaria = immaginaria;
}

// Metodi get
int Complessi::getReale(){
    return nReale;
}

int Complessi::getImmaginaria(){
    return nImmaginaria;
}

// Metodi set
void Complessi::setReale(int reale){
    nReale = reale;
}

void Complessi::setImmaginaria(int immaginaria) {
    nImmaginaria = immaginaria;
}

// Metodo per calcolare il modulo
double Complessi::modulo(){
    double complessi = sqrt(nReale * nReale + nImmaginaria * nImmaginaria);
    return complessi;
}

// Metodo per calcolare la fase 
double Complessi::fase(){
    double fase = atan2(nImmaginaria, nReale)*180./M_PI;
    return fase;
}

// Metodo per stampare le coordinate polari
/*void Complessi::stampaCoordinatePolari(){

    cout << "Coordinate polari:"<< endl;
    cout << "Modulo: " << modulo() << endl;
    cout << "Fase: " << fase() << endl;
}
*/
void Complessi::stampaCoordinate(){
    cout << "Reale:" << nReale <<" Immaginaria:" << nImmaginaria <<endl;
}

Complessi::~Complessi(){}

Complessi Complessi:: add ( Complessi y, Complessi z ){
    return Complessi (y.nReale+z.nReale , y.nImmaginaria+z.nImmaginaria);
}


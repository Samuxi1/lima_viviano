```mermaid

classDiagram
    class Complessi {
        - int nReale
        - int nImmaginaria
        + Complessi(int reale = 0, int immaginaria = 0)
        + int getReale() 
        + int getImmaginaria()
        + void setReale(int reale)
        + void setImmaginaria(int immaginaria)
        + double modulo()
        + double fase()
        + void stampaCoordinatePolari()
    }
    
    class main_cpp {
        + main()
    }

    class function_cpp {
        + Complessi::Complessi(int reale, int immaginaria)
        + Complessi::getReale()
        + Complessi::getImmaginaria()
        + Complessi::setReale(int reale)
        + Complessi::setImmaginaria(int immaginaria)
        + Complessi::modulo()
        + Complessi::fase()
        + Complessi::stampaCoordinatePolari()
    }
    
    class function_h {
        + Complessi
    }

    function_h <|-- function_cpp : include
    main_cpp --> function_h : include
    main_cpp --> function_cpp : utilizza

```
#ifndef FUNCTION_H
#define FUNCTION_H

class Complessi {
    private:
        int nReale;
        int nImmaginaria;

    public:
        // Costruttore
        Complessi(int reale = 0, int immaginaria = 0);

         // Metodi get
        int getReale();
        int getImmaginaria();

        // Metodi set
        void setReale(int reale);
        void setImmaginaria(int immaginaria);

        // Metodo calcolo modulo
        double modulo();

        // Metodo calcolo fase
        double fase();

        // Metodo per stampare le coordinate polari
        //void stampaCoordinatePolari();
        
        // Metodo per stampare le coordinate
        void stampaCoordinate();

        // Distruttore
        ~Complessi();

        // Sovraccarico dell'operatore =
        Complessi& operator=(const Complessi& z){
          nReale=z.nReale;nImmaginaria=z.nImmaginaria;return *this;  
        };

        friend Complessi operator+ ( const Complessi& y, const Complessi& z ) {
          return  Complessi (y.nReale+z.nReale, y.nImmaginaria+z.nImmaginaria);
        }
        friend Complessi operator- ( const Complessi& y, const Complessi& z ) {
          return Complessi (y.nReale - z.nReale, y.nImmaginaria - z.nImmaginaria);
        }
        friend Complessi operator* ( const Complessi& y, const Complessi& z ) {
          return Complessi (y.nReale * z.nReale - y.nImmaginaria * z.nImmaginaria, y.nImmaginaria * z.nReale + y.nReale * z.nImmaginaria );
        }

        friend Complessi operator/ ( const Complessi& y, const Complessi& z ) {
          return  Complessi ((y.nReale * z.nReale + y.nImmaginaria * z.nImmaginaria) /(z.nReale * z.nReale + z.nImmaginaria * z.nImmaginaria), (y.nImmaginaria * z.nReale - y.nReale * z.nImmaginaria) /(z.nReale * z.nReale + z.nImmaginaria * z.nImmaginaria) );
        }
        // Metodo per sommare due numeri complessi
        Complessi add ( Complessi y, Complessi z );

};

#endif


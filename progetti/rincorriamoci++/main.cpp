#include <iostream>
using namespace std;

// Prototipo
int scala_fino (int i, int min);

//Main
int main() {
  int k=3, j;
  cout << "Parto da: ";
  cin >> k;
  j= scala_fino (k,0);
  cout << "Sono partito da " << k << " mi sono fermato a " << j << ". Fine." <<endl;
  return j;
}

// Implementazione

int scala_fino (int i, int min){
  cout << i <<endl;
  if (i > min) {
    i--;
    return scala_fino(i,min);
  } else {
    return i;
  }

}

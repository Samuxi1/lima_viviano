# Istruioni per installare il software

## Compilazione

Per compilare eseguire semplicemente
---
g++ main.cpp -Wall
---

Viene generato un file di nome "a.out" che è il nostro file eseguibile

## Compilazione passo dopo passo

facendo riferimento a quanto scritto nel documento del [CNR sulla compilazione] (https://www-old.bo.cnr.it/corsi-di-informatica/corsoCstandard/Lezioni/16Linuxgcc.html)

Da seguire 4 passi:


### 1) preprocessing

Do il comando:


---
g++ main.cpp -E -o main.cpp.preprocessing
---

Che con il paramtro "-E" esegue solo la fase di preprocessing.
Per ottenere un file utilizzo "-o nome del file".

### 2) compilation

Con il parametro "-S" eseguo:
---

g++ main.cpp -S
---

E genera in automatico il file "main.s", se volessi dare un altro nome potrei usare "-o" come prima.

### 3) assembly

Do il comando:
---

g++ main.s -c
---

Parte dal file "main.s" e genera un file "main.o"

### 4) linking

Per creare l'eseguibile do il comando:
---

g++ main.o -Wall
---

E mi crea il file eseguibile di nome "a.out", se volessi cambiargli nome uso il parametro "-o".

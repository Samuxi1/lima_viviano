#ifndef FUNCTION_H
#define FUNCTION_H

#include <iostream>
#include <string>
#include <fstream> 
#include <vector>

using namespace std;

class HtmlFileManager {
public:
    HtmlFileManager(const string& fileName);
    void createFile();
    bool fileExist() const;
    static void showHelp();
    
    void setTitle(const string& newTitle);
    void addToHead(const string& element);
    void addToBody(const string& element);
    void addTableHeader(const vector<string>& headers);
    void addTableRow(const vector<string>& row);

private:
    string fileName;
    string title = "Pagina HTML Generata";
    vector<string> headElements;
    vector<string> bodyElements;
    vector<string> tableHeaders;
    vector<vector<string>> tableRows;
};

#endif

#include "function.h"

HtmlFileManager::HtmlFileManager(const string& fileName) : fileName(fileName) {}

void HtmlFileManager::createFile() {
    string fullFileName = fileName + ".html";
    cout << "Creazione file: " << fullFileName << endl;
    
    ofstream wfile(fullFileName);
    
    if (!wfile.is_open()) {
        cerr << "Errore: impossibile aprire il file " << fullFileName << " per la scrittura." << endl;
        return;
    }

    wfile << "<!DOCTYPE html>" << endl;
    wfile << "<html>" << endl;
    
    wfile << "<head>" << endl;
    wfile << "<title>" << title << "</title>" << endl;
    for (const auto& element : headElements) {
        wfile << element << endl;
    }
    wfile << "</head>" << endl;
    
    wfile << "<body>" << endl;
    for (const auto& element : bodyElements) {
        wfile << element << endl;
    }
    
    // Creazione tabella se presente
    if (!tableHeaders.empty() && !tableRows.empty()) {
        wfile << "<table id=\"myTable\" class=\"display\">" << endl;
        wfile << "<thead><tr>" << endl;
        for (const auto& header : tableHeaders) {
            wfile << "<th>" << header << "</th>";
        }
        wfile << "</tr></thead>" << endl;
        
        wfile << "<tbody>" << endl;
        for (const auto& row : tableRows) {
            wfile << "<tr>";
            for (const auto& cell : row) {
                wfile << "<td>" << cell << "</td>";
            }
            wfile << "</tr>" << endl;
        }
        wfile << "</tbody></table>" << endl;
    }
    
    wfile << "</body>" << endl;
    wfile << "</html>" << endl;
    wfile.close();
}

bool HtmlFileManager::fileExist() const {
    string fullFileName = fileName + ".html";
    ifstream rfile(fullFileName);
    bool exists = rfile.is_open();
    rfile.close();
    return exists;
}

void HtmlFileManager::showHelp() {
    cout << "Uso: ./program file_name" << endl;
    cout << "Usa solo il file_name senza estensione, sarà aggiunta automaticamente." << endl;
}

// Nuova funzione per modificare il titolo
void HtmlFileManager::setTitle(const string& newTitle) {
    title = newTitle;
}

// Nuova funzione per aggiungere elementi nella sezione head
void HtmlFileManager::addToHead(const string& element) {
    headElements.push_back(element);
}

// Nuova funzione per aggiungere elementi nella sezione body
void HtmlFileManager::addToBody(const string& element) {
    bodyElements.push_back(element);
}

// Funzione per aggiungere l'intestazione della tabella
void HtmlFileManager::addTableHeader(const vector<string>& headers) {
    tableHeaders = headers;
}

// Funzione per aggiungere una riga alla tabella
void HtmlFileManager::addTableRow(const vector<string>& row) {
    if (row.size() == tableHeaders.size()) {
        tableRows.push_back(row);
    } else {
        cerr << "Errore: la riga della tabella non corrisponde al numero di intestazioni." << endl;
    }
}

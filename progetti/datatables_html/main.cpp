#include "function.h"

int main(int argc, char** argv) {
    if (argc == 2) {
        HtmlFileManager manager(argv[1]);
        if (manager.fileExist()) {
            cout << "File già esistente, non sovrascrivo." << endl;
        } else {
            manager.setTitle("Elenco Alunni e Voti");
            manager.addToHead("<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css\">");
            manager.addToHead("<script src=\"https://code.jquery.com/jquery-3.6.0.min.js\"></script>");
            manager.addToHead("<script src=\"https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js\"></script>");
            manager.addToHead("<script>$(document).ready(function () { $('#myTable').DataTable(); });</script>");
            
            manager.addToBody("<h1>Elenco Alunni e Voti</h1>");
            manager.addTableHeader({"Nome", "Cognome", "Anno", "Classe", "Materia", "Data", "Voto (Media)", "Domande"});
            manager.addTableRow({"Mario", "Rossi", "2005", "3A", "Matematica", "12/02/2024", "8.5", "Domanda 1 (7), Domanda 2 (9)"});
            manager.addTableRow({"Giulia", "Bianchi", "2006", "4B", "Italiano", "05/01/2024", "7.0", "Domanda 1 (6), Domanda 2 (8)"});
            
            manager.createFile();
        }
    } else {
        HtmlFileManager::showHelp();
    }
    return 0;
}

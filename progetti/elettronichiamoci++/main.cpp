#include <iostream>
using namespace std;

int main() {
    double R1, R2, V_input;
    cout << "Inserisci il valore della resistenza R1: "<<endl;
    cin >> R1;
    cout << "Inserisci il valore della resistenza R2: "<<endl;
    cin >> R2;
    cout << "Inserisci il valore della tensione di input (V_input): "<<endl;
    cin >> V_input;
    double V_output = V_input * (R2 / (R1 + R2));
    double I= V_output / R2;
    cout << "Tensione di output (V_output): " << V_output << " volt " << endl;
    cout << "Corrente (I) che transita nelle resistenze: " << I << " ampere " << endl;

    return 0;
}


#include <iostream>
using namespace std;

int main() {
    int numero;
    cout << "inserisci un numero intero: ";
    cin >> numero;
    cout << "I divisori di " << numero << " sono:\n";
    for (int i = 1; i <= numero; ++i) {
        if (numero % i == 0) {
            cout << i << "\n";
        }

    }
    return 0;
}

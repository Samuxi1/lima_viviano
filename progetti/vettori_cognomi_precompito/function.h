#ifndef _FUNCTION_H
#define _FUNCTION_H
using namespace std;
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

//Prototipo
struct persona{
    string nome;
    string cognome;
    int anno_di_nascita;
};
persona inserisci_autore();
void stampa_autore(persona autore);
bool apri_file(string nome_file);
int conta_righe(string nome_txt);

#endif

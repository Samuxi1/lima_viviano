#ifndef _FUNCTION_H
#define _FUNCTION_H
using namespace std;
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <cmath>
//Prototipo
int max(int v[], int n);
int min(int v[], int n);
double media(int v[], int n);
double var(int v[], int n);
void riempi(int v[], int n);
void stampa(int v[], int n);
double correlazione(int v[], int v2[], int n);
#endif  

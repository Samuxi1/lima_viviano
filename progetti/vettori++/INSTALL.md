# compilazione

## main.o
g++ main.cpp -Wall -Wconversion -c -o main.o
## function.o
g++ -Wall -Wconversion function.cpp -c -o function.o
## a.out
g++ -Wall -Wconversion function.o main.o -o a.out
## eseguire un programma
./a.out

# Automatizzazione della compilazione con il makefile

Creando il makefile posso ridurre il tempo della compilazione
Digitando il comando make sul terminale il computer eseguire il primo passaggio descritto nel makefile

a.out: function.o main.o
	g++ -Wall -Wconversion main.o function.o -o a.out
function.o: function.cpp function.h
	g++ -Wall -Wconversion function.cpp -c -o function.o 
main.o: main.cpp function.h
	g++  main.cpp -Wall -Wconversion -c -o main.o
clean:
	rm -f *.o

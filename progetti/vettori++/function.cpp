#include "function.h"

//Implementazione

int max(int v[], int n){
    int ans=0;
    ans=v[0];
    for(int i=1; i<n;i++){
        if(v[i]> ans){
            ans=v[i];        
        }
    }
    return ans;
}

int min(int v[], int n){
    int ans=0;
    ans=v[0];
    for(int i=1; i<n;i++){
        if(v[i]< ans){
            ans=v[i];        
        }
    }
    return ans;
}

double media(int v[], int n){
    double ans=0.;
    int somma=0;
    for(int i=0; i<n; i++){
        somma += v[i];
    }
    ans= somma/ (double)n;
    return ans; 
} 

double var(int v[], int n){
    double ans=0.;
    double media_v=0.;
    double somma=0.;
    
    for(int i=0; i< n; i++){
        somma+=((double)v[i]-media_v)*((double)v[i]-media_v);
    }
    ans=somma/(double) n;
    return ans;
}

double correlazione(int v[], int v2[], int n){
    double ans =0., m1, m2, var1, var2,denominatore;
    m1=media(v,n);
    m2=media(v2,n);
    var1=var(v,n);
    var2=var(v2,n);
    denominatore = sqrt(var1)*sqrt(var2);
    for(int i=0; i<n; i++){
        ans=ans+ ((double)v[i]-m1)*((double)v2[i]-m2);
    }
    ans=ans/(double)n;
    ans=ans/denominatore;
    return ans;
    }

void riempi(int v[], int n){    
    for(int i=0; i< n; i++){
        v[i]=rand()%300+1;
    }
}

void stampa(int v[], int n){
    for(int i=0; i<n; i++){
        cout << "|"<<v[i]<< "|"<<endl;
        cout << "----"<<endl;
    }
}

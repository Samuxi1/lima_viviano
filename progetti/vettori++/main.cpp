
#include "function.h"

int main(){
    srand((unsigned int)time(NULL));
    int n=0;
    cout << "Quanti elementi vuoi nel tuo array? "<<endl;
    cin >> n;
    cout << "---"<<endl;
    int v[n];
    int v2[n];
    riempi(v,n);
    riempi(v2,n);
    stampa(v,n);
    cout<< "Numero elementi del Vettore: "<< n <<endl;
    cout<< "Numero massimo del Vettore: "<< max(v,n) <<endl;
    cout<< "Numero minimo del Vettore: "<< min(v,n) <<endl;
    cout<< "La media vale: "<< media(v,n) <<endl;
    cout<< "La varianza vale: "<< var(v,n) <<endl;
    cout << "La correlazione tra v e v2 vale: "<< correlazione(v,v2,n)<<endl;
    
    return 0;
}


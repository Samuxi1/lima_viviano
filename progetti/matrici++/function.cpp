#include "function.h"

//Implementazione
void stampa_matrice(double matrice[MAX][MAX],int riga, int colonna){
    for(int k=0; k < colonna +1; k++){
        cout<< "_________"<<right;
    }
    for(int i=0; i < riga; i++){
        cout<<endl;
        cout<<i+1<<"\t";
        for(int j=0; j < colonna; j++){
            if(j == colonna -1){
                cout << "| "<< matrice[i][j]<<"\t"<<"|";
            }else{
                cout << "| "<<matrice[i][j]<<"\t";
            }
      
        }
    cout<<endl;
    for(int k=0; k < colonna +1; k++){
        cout<<"__________"<<right;
    }
    }
      
}

void riempi_matrice(double matrice[MAX][MAX],int riga, int colonna){
    for(int i=0; i< riga; i++){
        for(int j=0; j < colonna; j++){
            matrice[i][j]=rand()%10+1;
        }
    }
}

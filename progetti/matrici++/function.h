#ifndef _FUNCTION_H
#define _FUNCTION_H
using namespace std;
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
const int MAX=200;
//Prototipo
void stampa_matrice(double matrice[MAX][MAX], int riga, int colonna);
void riempi_matrice(double matrice[MAX][MAX], int riga, int colonna);
#endif  

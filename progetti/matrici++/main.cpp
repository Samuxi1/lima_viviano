#include "function.h"

int main(){
    srand((unsigned int)time(NULL));
    double matrice [MAX][MAX]{0};
    int riga,colonna;
    riga=rand()%1+5;
    colonna=rand()%1+10;
    riempi_matrice(matrice,riga,colonna);
    cout << " Matrice di " << riga << " x "<< colonna <<endl;
    stampa_matrice(matrice,riga,colonna);
    return 0;
}


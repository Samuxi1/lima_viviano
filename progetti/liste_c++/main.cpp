#include "function.h"

struct nodo {
	int n;
	nodo * next;
};

void stampa(nodo *);
void add(nodo * testa, int valore); // deve aggiungere `valore` in fondo alla lista
void nuova(nodo *&, int valore); // TODO: implementare, se vuota cera la lista
bool empty(nodo *); // TODO: implementare: se NULL restituisce true

int main(int argc, char * argv[]) {
	srand((unsigned int)time(NULL));
	nodo *a = NULL;
	cout << "Lista di valori"<<endl;
	// Creo il mio primo nodo (vagone)
	nodo *temporanea = new nodo;
	temporanea->n = 1; // metto un valore
	temporanea->next = NULL; // impongo NULL (fine del treno)
	a = temporanea; // `p` punta al primo vagone.
	add(a,3000);
	for(int i=0; i<10;i++) {
		add(a, i*10+8);
	}
	// Stampo la lista
	stampa(a);
  cout << "Creazione nuova lista"<<endl;
  nodo *nuovo = NULL;
  nuova(nuovo, 10);
  stampa(nuovo);  
  return 0;
}

void stampa(nodo * da_stampare) {
	if(da_stampare != NULL ) {
		cout << "Valore nodo: " << da_stampare->n << endl;
		stampa(da_stampare->next);
	} else {
		cout << "Fine lista"  << endl;
	}
}

void add(nodo * testa, int valore) {
	// TODO: controllare valore di testa
	nodo * controllore;
	// arrivare in fondo alla lista
	controllore = testa;
	while(controllore->next != NULL) {
		controllore = controllore->next;
	}
	// creo il vagone temporaneo e Attacco il vagone
	controllore->next = new nodo;
	// inserisco il valore
	controllore->next->n = valore;
	// mi assicuro che sia NULL il puntatore a next
	controllore->next->next = NULL;
}


bool empty(nodo *testa) {
    if (testa == NULL){
      return true;
    }
    return false;
}

void nuova(nodo *&testa, int valore) {
  if (empty(testa)) {
    // Crea il primo nodo se la lista è vuota
    nodo *temp = new nodo;
    temp->n = valore;
    temp->next = NULL;
    testa = temp;
  }else{
    // Se la lista non è vuota, usa il metodo `add` per aggiungere in fondo
    add(testa, valore);
  }
}


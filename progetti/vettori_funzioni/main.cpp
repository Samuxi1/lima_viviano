#include <iostream>
#define N 20
using namespace std;

// Prototipo
void stampa(int v[], int n);
// Utilizzo

int main(){
    int v[N];
    int n=2;
    srand ((unsigned)time(NULL));
    v[0]=5;
    v[1]=4;
    stampa(v, n);
    return 0;
}

// Implementazione

void stampa(int v[],int n){
    for(int i=0; i<n; i++){
        cout<< v[i] <<endl;
    }
}



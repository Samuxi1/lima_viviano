#include "function.h"

int main(int argc, char * argv[]){
  if(argc != 2){
    help("Non hai utilizzato correttamente il programma");
    return 1;
  }
  ifstream thefile;
  string nome_file = argv[1];
  char c; //  carattere letto
  char c1 = argv[1] [0]; // carattere da scambiare
  char c2 = argv[1] [1]; // carattere da scambiare
  thefile.open(nome_file);
  if(thefile.is_open()){
    help("File aperto correttamente ");
    while(thefile.get(c)){
      if((c == c1) or (c == c2)){
        if (c == c1){
          c = c2;
        }else{
          c = c1;
        }
      }
      cout << c;
    }
    thefile.close();
  }else{
    help("File *NON* aperto correttamente");
  }

  return 0;
}


#include "function.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

// Funzione per ottenere un nome casuale
string get_nome() {
    string tmp[] = {"Luca", "Paolo", "Francesca", "Laura", "Camilla"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

string get_cognome() {
    string tmp[] = {"Lucchini", "Meloni", "Silvestri", "Bennato", "Verdi", "Rossi"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

string get_data() {
    return to_string(get_casuale_tra(1, 28)) + "/" + to_string(get_casuale_tra(1, 12)) + "/" + to_string(get_casuale_tra(2000, 2024));
}

string get_classe() {
    string tmp[] = {"1ait", "2bit", "3cit", "4ait", "4bit", "4cafm", "3bcat", "2aafm"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

double get_valore_voto() {
    return ((double)get_casuale_tra(4, 20)) / 2.0;
}

string get_materia() {
    string tmp[] = {"Informatica", "Matematica", "Italiano", "Storia", "Scienze motorie", "Francese"};
    return tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0]) - 1)];
}

int get_anno() {
    return get_casuale_tra(1960, 2020);
}

Persona get_persona() {
    Persona p;
    p.nome = get_nome();
    p.cognome = get_cognome();
    p.anno = get_anno();
    p.classe = get_classe();
    p.voti = get_lista_voti();
    return p;
}

Voto get_voto() {
    Voto v;
    v.materia = get_materia();
    v.data = get_data();
    v.domande = get_lista_domande();

    // Calcolo della media
    double somma = 0;
    int count = 0;
    ListaDomande *curr = v.domande;
    while (curr != NULL) {
        somma += curr->dato.valutazione;
        count++;
        curr = curr->next;
    }
    v.numero = somma / count;
    return v;
}

ListaDomande *get_lista_domande() {
    ListaDomande *ans = new ListaDomande;
    ans->dato = {"Domanda 1", get_valore_voto()};
    ans->next = NULL;

    ListaDomande *tmp = ans;
    for (int i = 1; i < get_casuale_tra(3, 5); i++) {
        tmp->next = new ListaDomande;
        tmp = tmp->next;
        tmp->dato = {"Domanda " + to_string(i + 1), get_valore_voto()};
        tmp->next = NULL;
    }
    return ans;
}

ListaVoti *get_lista_voti() {
    ListaVoti *ans = new ListaVoti;
    ans->dato = get_voto();
    ans->next = NULL;

    ListaVoti *tmp = ans;
    for (int i = 0; i < get_casuale_tra(5, 10); i++) {
        tmp->next = new ListaVoti;
        tmp = tmp->next;
        tmp->dato = get_voto();
        tmp->next = NULL;
    }
    return ans;
}

ListaPersone *get_lista_persone() {
    ListaPersone *ans = new ListaPersone;
    ans->dato = get_persona();
    ans->next = NULL;

    ListaPersone *tmp = ans;
    for (int i = 0; i < get_casuale_tra(1, 5); i++) {
        tmp->next = new ListaPersone;
        tmp = tmp->next;
        tmp->dato = get_persona();
        tmp->next = NULL;
    }
    return ans;
}

int get_casuale_tra(int da, int a) {
    return (rand() % (a - da + 1)) + da;
}

// Funzioni di stampa
void stampa(const ListaPersone *l) {
    if (l != NULL) {
        stampa(l->dato);
        stampa(l->next);
    }
}

void stampa(Persona p) {
    cout << "======= Persona =============" << endl;
    cout << "Nome: " << p.nome << "; Cognome: " << p.cognome << "; Anno: " << p.anno << "; Classe: " << p.classe << endl;
    cout << "\t======= Voti =============" << endl;
    stampa(p.voti);
}

void stampa(const ListaVoti *l) {
    if (l != NULL) {
        stampa(l->dato);
        stampa(l->next);
    }
}

void stampa(Voto v) {
    cout << "\tMateria: " << v.materia << "; Data: " << v.data << "; Voto (media): " << v.numero << endl;
    cout << "\t\tDettaglio domande:" << endl;
    stampa(v.domande);
}

void stampa(const ListaDomande *l) {
    if (l != NULL) {
        cout << "\t\t" << l->dato.testo << ": " << l->dato.valutazione << endl;
        stampa(l->next);
    }
}
